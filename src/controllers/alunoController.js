const { Aluno } = require("../models/mainModels");

module.exports = {
    postAluno: async (req, res) => {
       try {
        const alunos = await Aluno.create({
            nome: req.body.nome,
            idade: req.body.idade,
            nome_mae: req.body.nome_mae,
            nome_pai: req.body.nome_pai,
            cpf: req.body.cpf
        });
        return res.status(201).json(alunos)
       } catch (error) {
        return res.status(500).json(error)
       }
    },

    getAllAlunos: async (req, res) => {
        try {
         const alunos = await Aluno.findAll()
         return res.status(200).json(alunos)
        } catch (error) {
        return res.status(500).json(error)
        }
    },

    getAluno: async (req, res) => {
        try {
        const { cpf } = req.params;
        const alunos = await Aluno.findOne({
            where:{
                cpf: cpf,
            }
        });
        if(!alunos){
            return res.status(404).json({error: "Não foi possível encontrar o aluno(a)!"})
        }
        return res.status(200).json(alunos)
        } catch (error) {
        return res.status(500).json(error)   
        }
    },

    deleteAluno: async (req, res) => {
        try {
            const alunos = await Aluno.destroy({
                where: {
                    cpf: req.params.cpf
                }
            });
            if (!alunos) {
                return res.status(404).json({error: "Não foi possível encontrar o aluno(a)!"})
            }
            return res.status(200).json(alunos)
        } catch (error) {
            return res.status(500).json(error)
        }
    },

    editAluno: async (req, res) => {
        try {
            const { cpf } = req.params;
            const { nome, idade, nome_mae, nome_pai} = req.body;
            const alunos = await Aluno.findOne({
                where:{
                    cpf: cpf,
                }
            });
            if (!alunos) {
                return res.status(404).json({error: "Não foi possível encontrar o aluno(a)!"})
            };
            const alunoEditado = await alunos.update({ nome, idade, nome_mae, nome_pai});
            return res.status(200).json(alunoEditado)
        } catch (error) {
            return res.status(500).json(error)
        }
    },

    turmaAluno: async (req, res) => {
        try {
            const { cpf } = req.params;
            const alunos = await Aluno.findOne({
                where:{
                    cpf: cpf
                }
            });
            if (!alunos) {
                return res.status(404).json({error: "Não foi possível encontrar o aluno(a)!"});
            }
            const alunoUpdated = await alunos.update({
                TurmaNumero: req.params.TurmaNumero
            });
            return res.status(200).json(alunoUpdated);
        } catch (error) {
            return res.status(500).json(error)
        }
    }
}

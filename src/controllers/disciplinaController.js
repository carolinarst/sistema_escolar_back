const { Disciplina } = require("../models/mainModels");

module.exports = {
    postDisciplina: async (req, res) => {
        try {
            const disciplinas = await Disciplina.create({
                codigo: req.body.codigo,
                nome: req.body.nome
            });

            return res.status(201).json(disciplinas)
        } catch (error) {
            return res.status(500).json(error);
        }
    },

    getDisciplina: async (req, res) => {
        try {
            const disciplinas = await Disciplina.findByPk(req.params.codigo);
            if(disciplinas) {
                return res.status(200).json(disciplinas)
            } else {
                return res.status(404).json({error: "Não foi possível encontrar a disciplina!"})
            }
        } catch (error) {
            return res.status(500).json(error);
        }
    },

    getAllDisciplina: async (req, res) => {
        try {
            const disciplinas = await Disciplina.findAll({});
            if (disciplinas) {
                return res.status(200).json(disciplinas);
            } else{
                return res.status(404).json({error: "Nenhuma disciplina encontrada!"})
            }
        } catch (error) {
            return res.status(500).json(error)
        }
    },

    deleteDisciplina: async (req, res) => {
        try {
            const disciplinas = await Disciplina.destroy({
                where: {
                    codigo: req.params.codigo
                }
            });

            if(!disciplinas) {
                return res.status(404).json({error: "Não foi possível encontrar a disciplina!"})
            }
            return res.status(200).json(disciplinas);
        } catch (error) {
            return res.status(500).json(error);            
        }
    },

    editDisciplina: async (req, res) => {
        try {
            const { codigo } = req.params;
            const { nome } = req.body;

            const disciplinas = await Disciplina.findByPk(codigo);
            if (!disciplinas) {
                return res.status(404).json({error: "Não foi possível encontrar a disciplina!"})
            } 
            const disciplinaEditada = await disciplinas.update({ nome });
            return res.status(200).json(disciplinaEditada)
        } catch (error) {
            return res.status(500).json(error)
        }
    }
}
const { Turma } = require("../models/mainModels");

async function listarTurmas(req, res){
    try{
        const turmas = await Turma.findAll();
        return res.status(200).json(turmas);
    }catch{
        return res.status(400).json({error: "Não foi possível listar as turmas."});
    };
};

async function criarTurma(req,res){
    try{
        const { ano,turno,serie } = req.body;
        const turma = await Turma.create({
            ano,
            turno,
            serie
        });

        console.log(turma);

        return res.status(200).json(turma);
    }catch{
        return res.status(400).json({error: "Não foi possível criar a turma."});
    };
};

async function turmaByNum(req, res){
    try{
        const { numero } = req.params;
        const turma = await Turma.findByPk(numero);

        if(!turma){
            return res.status(404).json({error:"Não foi possível encontrar a turma."});
        }

        return res.status(200).json(turma);
    }catch{
        return res.status(400).json({error:"Erro ao procurar turma."})
    }
}

async function patchTurma(req,res){
    try {
        const { numero } = req.params;
        const { ano, turno, serie } = req.body;

        const turmas = await Turma.findByPk(numero);
        if (!turmas) {
            return res.status(404).json({error: "Não foi possível encontrar a turma!"})
        } 
        const turmaEditada = await  turmas.update({ ano, turno , serie });
        return res.status(200).json(turmaEditada);
    } catch (error) {
        return res.status(500).json(error)
    }
}

async function deleteTurma(req, res){
    try {       
        const { numero } = req.params;

        const turmas = await Turma.destroy({
            where: {
                numero: numero
            }
        });

        if(!turmas) {
            return res.status(404).json({error: "Não foi possível encontrar a turma!"})
        }
        return res.status(200).json(turmas);
    } catch (error) {
        return res.status(500).json(error);            
    }

}

module.exports = {
    listarTurmas,
    criarTurma,
    turmaByNum,
    patchTurma,
    deleteTurma
};
const { Avaliacao } = require("../models/mainModels");

//Cadastrar uma avaliação
async function registrarAvaliacao(req, res) {
    try {
        
        const { nome, notaMaxima, peso } = req.body;

        const avaliacao = await Avaliacao.create({
            nome,
            notaMaxima,
            peso,
        });

        return res.status(201).json(avaliacao);

    } catch (error) {
        return res.status(500).json({ error: "Ocorreu um erro ao tentar criar a avaliação." });
    }
}

//Editar uma avaliação
async function editarAvaliacao(req, res) {
    try {
        const { avaliacaoId } = req.params;
        const { nome, notaMaxima, peso } = req.body;
        
        const avaliacao = await Avaliacao.findByPk(avaliacaoId);

        if (!avaliacao){
            return res.status(404).json({ error: "Avaliação não encontrada."});
            }

        const avaliacaoEditada = await avaliacao.update({ nome, notaMaxima, peso});

        res.status(200).json(avaliacaoEditada);

    } catch (error) {
        return res.status(500).json({error: "Erro ao editar avaliação."})
    }
}

//Listar avaliações
async function listarAvaliacoes(req, res) {
    try {
        const avaliacoes = await Avaliacao.findAll();

        return res.status(201).json(avaliacoes);

    } catch (error) {
        return res.status(500).json({ error: "Não foi possível listar as avaliações." });
        
    }
    
}

//Deletar uma avaliação
async function deletarAvaliacao(req, res) {
    try {
        const { avaliacaoId } = req.params;

        const avaliacao = await Avaliacao.destroy({ 
            where: { 
                id: avaliacaoId, 
            } 
        });

        if (!avaliacao){
            return res.status(404).json({ error: "Avaliação não encontrada."});
            }

        return res.status(202).json();
        
    } catch (error) {
        return res.status(500).json({ error: "Erro ao tentar deletar a avaliação." })
    }
    
}

module.exports = {
    registrarAvaliacao,
    editarAvaliacao,
    deletarAvaliacao,
    listarAvaliacoes
}
const { Aluno, Usuario } = require("../models/mainModels");

async function addUser(request, response) {
    try {
        const { cargo, email, senha, nome, cpf } = request.body;
        const usuario = await Usuario.create({
            cargo,
            email,
            senha,
            nome,
            // idade,
            // nome_mae,
            // nome_pai,
            cpf,
        });
        return response.status(201).json(usuario);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível cadastrar o usuário!"});
    }
}

async function editUser(request, response) {
    try {
        const { email, senha, nome, cpf } = request.body;
        request.user.update({
            email,
            senha,
            nome,
            // idade,
            // nome_mae,
            // nome_pai,
            cpf,
        });
        return response.status(200).json(request.user);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível editar o usuário!"});
    }
}

async function listUsers(request, response) {
    try {
        if(request.loggedUser.cargo == 0) {
            const secretarios = await Usuario.findAll({ where: {cargo: 1}});
            const coordenadores = await Usuario.findAll({ where: {cargo: 2}});
            const professores = await Usuario.findAll({ where: {cargo: 3}});
            var usuarios = secretarios.concat(coordenadores.concat(professores));
        }
        if(request.loggedUser.cargo == 1) {
            var usuarios = await Aluno.findAll({ where: {cargo: 4}});
        }
        if(request.loggedUser.cargo == 2) {
            var usuarios = await Usuario.findAll({ where: {cargo: 3}});
        } 
        return response.status(200).json(usuarios);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível listar os usuários!"});
    }
}

async function getUserById(request, response) {
    try {
        const user = request.user;
        return response.status(200).json(user);
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível encontrar o usuário!"});
    }
}

async function deleteUser(request, response) {
    try {
        const user = request.user;
        await user.destroy();
        return response.status(200).send();
    } catch (error) {
        return response.status(400).json({ error: "Não foi possível excluir o usuário!"});
    }
}

module.exports = {
    addUser,
    editUser,
    listUsers,
    getUserById,
    deleteUser,
}
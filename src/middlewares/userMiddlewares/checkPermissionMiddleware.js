const Usuario = require("../../models/mainModels");

async function checkPermission(request, response, next, cargo) {
    try {
        if ([1,2,3].includes(cargo) && request.loggedUser.cargo == 0) {
            return next()
        }
        if ([1].includes(cargo) && request.loggedUser.cargo == 4) {
            return next()
        }
        if ([2].includes(cargo) && request.loggedUser.cargo == 3) {
            return next()
        }
        return response.status(403).json({ error: "Usuário não tem permissão!" });
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar permissão do usuário!" });
    }
}

async function checkPermissionByUser(request, response, next) {
    const cargo = request.user.cargo;
    return checkPermission(request, response, next, cargo);
}

async function checkCreatePermission(request, response, next) {
    const { cargo } = request.body;
    return checkPermission(request, response, next, cargo);
}

module.exports = {
    checkPermissionByUser,
    checkCreatePermission,
}
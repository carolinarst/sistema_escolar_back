const { Aluno, Usuario } = require("../../models/mainModels");

async function findUser(request, response, next) {
    try {
        const { id } = request.params;
        var user = await Usuario.findByPk(id);
        if(!user) {
            user = await Aluno.findByPk(id);
        }
        if(!user) {
            return response.status(400).json({ error: "Usuário não encontrado!"});
        }
        request.user = user;
        next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar se usuário existe!" });
    }
}

module.exports = findUser;
const Usuario = require("../../models/mainModels");

async function checkCredentials(request, response, next) {
    try {
        const { email, senha } = request.headers;
        const usuario = await Usuario.findOne({ where: {email,senha}});
        if(usuario == null) {
            return response.status(400).json({ error: "Usuário não existe" });
        }
        request.loggedUser = usuario
        return next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao checar se usuário existe!" });
    }
}

module.exports = checkCredentials;
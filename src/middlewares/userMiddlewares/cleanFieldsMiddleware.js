async function cleanFields(request, response, next, cargo) {
    try {
        if(cargo != 4) {
            request.body.idade = 0;
            request.body.nome_pai = "Ignorado";
            request.body.nome_mae = "Ignorado";
        }
        return next();
    } catch (error) {
        return response.status(500).json({ error: "Erro ao identificar cargo!" });
    }
}

async function cleanEditFields(request, response, next) {
    const cargo = request.user.cargo;
    return cleanFields(request, response, next, cargo);
}

async function cleanCreateFields(request, response, next) {
    const { cargo } = request.body;
    return cleanFields(request, response, next, cargo);
}

module.exports = {
    cleanFields,
    cleanEditFields,
    cleanCreateFields,
}
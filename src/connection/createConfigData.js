const { Sequelize } = require("sequelize")

const sequelize = new Sequelize({
    dialect: "sqlite",
    storage: "./src/data/database.sqlite",
    define: {
        timestamps: true,
        underscored: true,
        underscoredAll: true,
        force: true
      },
})

// Teste de conexão do sequelize
const connection = async ()=>{
    try {
        await sequelize.authenticate({force: true})
        console.log("Database online")
    } catch (error) {
        console.log(error)
    }
} 

connection()

module.exports = {
    sequelize,
    connection
}
const { sequelize } = require("../connection/createConfigData");
const { Usuario } = require("../models/mainModels");

module.exports = {
   createDatabase: async () => {
     try {
       await sequelize.sync({ alter: true });
       await Usuario.create({
        cargo: 0,
        email: "admin@admin.com",
        senha: "admin",
        nome: "Admin",
        idade: 0,
        nome_mae: "Ignorado",
        nome_pai: "Ignorado",
        cpf: "000.000.000-00",
       });
       console.log("DataBase created");
     } catch (error) {
       console.log(error);
     }
   },
 };
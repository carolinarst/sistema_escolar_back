const { sequelize } = require("../connection/createConfigData");
const { DataTypes } = require("sequelize");
const { v4: uuidv4 } = require("uuid");

const Aluno = sequelize.define("Aluno", {
    nome: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1,60],
        },
    },

    idade: {
         type: DataTypes.INTEGER,
         allowNull: false,
         validate: {
             min: 0,
             max: 99,
         }
    },

    nome_mae: {
         type: DataTypes.STRING,
         allowNull: false,
         validate: {
             len: [1,60],
        }
    },

    nome_pai: {
         type: DataTypes.STRING,
         allowNull: false,
         validate: {
             len: [1,60],
         }
    },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            is: /^\d{3}\.\d{3}\.\d{3}-\d{2}$/
        }
    }
});

const Avaliacao = sequelize.define("Avaliacao", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false,
    },
    notaMaxima: {
        type: DataTypes.INTEGER,
        allowNull: false,    
    },
    peso: {
        type: DataTypes.FLOAT,
        defaultValue: 1,
    },
});

const Disciplina = sequelize.define("Disciplina", {
    codigo: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        primaryKey: true,
        validate: {
            is: ["[0-9]"],
            len: 3,
        }
    },
    nome: {
        type: DataTypes.STRING,
        allowNull: false,
    },
});

const Nota = sequelize.define("Nota", {
    valorNota: {
        type: DataTypes.FLOAT,
        defaultValue: 0
    }, 
});

const Turma = sequelize.define("Turma",{
    numero: {
        type: DataTypes.INTEGER,
        validate:{is:[/^\d{3}$\^/]},
        autoIncrement: true,
        primaryKey:true,
        allowNull:false
    },
    /*
    id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
        
    },*/
    ano: {
        type: DataTypes.STRING,
        allowNull:false,
    },
    turno: {
        type: DataTypes.STRING,
        allowNull: false
    },
    serie:{
        type:DataTypes.INTEGER,
        allowNull: false
    }
});

const Usuario = sequelize.define("Usuario", {
    id: {
        type: DataTypes.UUIDV4,
        defaultValue: () => uuidv4(),
        primaryKey: true,
    },


    // 0: admin | 1: secretario | 2: coordenador | 3: professor | 4: aluno
    cargo: {
        type: DataTypes.INTEGER,
        validate: {
            min: 0,
            max: 4,
        }
    },
    
    email: {
        type: DataTypes.STRING,
        primaryKey: true,
        unique: true,
        validate: {
            isEmail: true
        }
    },

    senha: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [4,64],
        }
    },

    nome: {
        type: DataTypes.STRING,
        allowNull: false,
        validate: {
            len: [1,60],
        },
    },

    // idade: {
    //     type: DataTypes.INTEGER,
    //     allowNull: false,
    //     validate: {
    //         min: 0,
    //         max: 99,
    //     }
    // },

    // nome_mae: {
    //     type: DataTypes.STRING,
    //     allowNull: false,
    //     validate: {
    //         len: [1,60],
    //     }
    // },

    // nome_pai: {
    //     type: DataTypes.STRING,
    //     allowNull: false,
    //     validate: {
    //         len: [1,60],
    //     }
    // },

    cpf: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
            is: /^\d{3}\.\d{3}\.\d{3}-\d{2}$/
        }
    }
});

Turma.hasMany(Aluno, {
    // foreignKey: "turmaId",
    onDelete: "CASCADE"
});
Aluno.belongsTo(Turma);

Turma.hasMany(Disciplina, {
    // foreignKey: "turmaId",
    onDelete: "CASCADE"
});
Disciplina.belongsTo(Turma);

Disciplina.hasMany(Avaliacao, {
    onDelete: "CASCADE"
});
Avaliacao.belongsTo(Disciplina);

// Definir a associação entre Disciplina e Usuario
Disciplina.hasMany(Usuario, { onDelete: 'CASCADE' });
Usuario.belongsTo(Disciplina);

module.exports = {
    Aluno,
    Avaliacao,
    Disciplina,
    Nota,
    Turma,
    Usuario,
};
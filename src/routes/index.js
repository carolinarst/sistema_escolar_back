const Router = require("express");
const router = Router();

// Rotas Avaliações
const avaliacaoRoutes = require("./avaliacaoRoute");
router.use('/avaliacao', avaliacaoRoutes);

// Rotas Disciplinas
const disciplinaRoute = require("./disciplinaRoute")
router.use("/disciplina", disciplinaRoute);

//Rotas Aluno
const alunoRoutes = require("./alunoRoute")
router.use("/aluno", alunoRoutes);

//Rotas Usuários
const usuarioRoutes = require("./usuarioRoute")
router.use("/usuario", usuarioRoutes)

//RotasTurmas
const turmasRoutes = require("./turmas.routes")
router.use("/turmas", turmasRoutes);

//Rotas Notas

module.exports = router;

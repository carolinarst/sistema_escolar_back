const express = require("express");
const avaliacaoRoutes = express.Router();

//Inclusão dos Middlewares (?)

//Inclusão dos Controllers
const avaliacaoController = require('../controllers/avaliacaoController');

//Registrar Avaliação
avaliacaoRoutes.post('/', (req, res) => avaliacaoController.registrarAvaliacao(req, res));

//Editar Avaliação
avaliacaoRoutes.patch("/:avaliacaoId", (req, res) => avaliacaoController.editarAvaliacao(req, res));

//Deletar Avaliação
avaliacaoRoutes.delete("/:avaliacaoId", (req, res) => avaliacaoController.deletarAvaliacao(req, res));

//Listar Avaliações
avaliacaoRoutes.get("/", (req, res) => avaliacaoController.listarAvaliacoes(req, res));

module.exports = avaliacaoRoutes;
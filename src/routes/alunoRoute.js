const express = require('express');
const alunoRoutes = express.Router();
const { 
    postAluno,
    getAllAlunos,
    getAluno,
    deleteAluno,
    editAluno,
    turmaAluno
} = require("../controllers/alunoController")


alunoRoutes.post("/", (req, res) =>
    postAluno(req, res)
);

alunoRoutes.get("/", (req, res) =>
    getAllAlunos(req, res)
);

alunoRoutes.get("/:cpf", (req, res) =>
    getAluno(req, res)
);

alunoRoutes.delete("/:cpf", (req, res) =>
    deleteAluno(req, res)
);

alunoRoutes.patch("/:cpf", (req, res) =>
    editAluno(req, res)
);

alunoRoutes.patch("/:cpf/:TurmaNumero", (req, res) =>
    turmaAluno(req, res)
)


module.exports = alunoRoutes;
const express = require('express');
const usuarioRoutes = express.Router();

//Inclusão de Middlewares
const checkCredentials = require('../middlewares/userMiddlewares/checkCredentialsMiddleware');
const {checkPermissionByUser,checkCreatePermission} = require('../middlewares/userMiddlewares/checkPermissionMiddleware');
const {cleanEditFields, cleanCreateFields } = require('../middlewares/userMiddlewares/cleanFieldsMiddleware');
const findUser = require('../middlewares/userMiddlewares/findUserMiddleware');

//Inclusão do Controller
const usuarioController = require('../controllers/usuarioController');

//Cadastrar Usuário
usuarioRoutes.post(
    '/cadastrar',
    checkCredentials,
    checkCreatePermission,
    cleanCreateFields,
    (request, response) => usuarioController.addUser(request, response)
);

//Login
usuarioRoutes.post(
    '/login',
    checkCredentials,
    (request,response) => response.status(200).json(request.loggedUser),
);

//Buscar usuário por id
usuarioRoutes.get(
    '/:id', 
    checkCredentials,
    findUser, 
    checkPermissionByUser,
    (request, response) => usuarioController.getUserById(request, response)
);

//Editar usuário
usuarioRoutes.patch(
    '/:id',
    checkCredentials,
    findUser, 
    checkPermissionByUser,
    cleanEditFields,
    (request, response) => usuarioController.editUser(request, response)
);

//Listar Usuários
usuarioRoutes.get(
    '/',
    checkCredentials,
    (request, response) => usuarioController.listUsers(request, response));

//Excluir Usuário
usuarioRoutes.delete(
    '/:id',
    checkCredentials,
    findUser,
    checkPermissionByUser,
    (request, response) => usuarioController.deleteUser(request, response)
);


module.exports = usuarioRoutes;
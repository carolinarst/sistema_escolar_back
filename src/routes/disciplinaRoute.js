const express = require("express");
const router = express.Router();

const {
    postDisciplina,
    getDisciplina,
    getAllDisciplina,
    deleteDisciplina,
    editDisciplina,
} = require ("../controllers/disciplinaController")

router.post("/", (req, res) => {
    postDisciplina(req, res);
});

router.get("/:codigo", (req, res) => {
    getDisciplina(req, res);
});

router.get("/", (req, res) => {
    getAllDisciplina(req, res);
});

router.delete("/:codigo", (req, res) => {
    deleteDisciplina(req, res);
});

router.patch("/:codigo", (req, res) => {
    editDisciplina(req, res);
});

module.exports = router;


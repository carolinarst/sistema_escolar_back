const express = require('express');
const turmasRoutes = express.Router();
const turmasController = require('../controllers/turmasController');

turmasRoutes.post('/criarturma', async (req, res) => turmasController.criarTurma(req, res));

turmasRoutes.get('/listarturmas',(req, res) => turmasController.listarTurmas(req,res));

turmasRoutes.get("/:numero", (req, res) => turmasController.turmaByNum(req, res));

turmasRoutes.patch("/:numero", (req, res) => turmasController.patchTurma(req, res));

turmasRoutes.delete("/:numero", (req, res) => turmasController.deleteTurma(req, res));

module.exports = turmasRoutes;
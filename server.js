const express = require("express")
const app = express()
const port = 3000;
const router = require('./src/routes/index')
const { createDatabase } = require("./src/database/createData");
const bodyParser = require('body-parser');
const  cors = require('cors');


app.use(cors());
app.use(bodyParser.json());
app.use(express.json());
app.use(router);

app.listen(port, ()=>{
    console.log("porta 3000")
})

 try {
    createDatabase()   
    } catch (error) {
     console.log(error)
}